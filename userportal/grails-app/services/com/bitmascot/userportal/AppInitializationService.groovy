package com.bitmascot.userportal

class AppInitializationService {

    static initialize() {
        initUser()
    }

    private static initUser() {
        if (User.count() == 0) {
            User user = new User()
            user.firstName = "System"
            user.lastName = "Administrator"
            user.email = "admin@localhost.local"
            user.password = "admin"
            user.birthDate = "2000-10-20"
            user.userType = GlobalConfig.USER_TYPE.ADMINISTRATOR
            user.save(flash: true)
        }
    }
}
