package com.bitmascot.userportal

import grails.web.servlet.mvc.GrailsParameterMap

import java.text.SimpleDateFormat

class UserService {

    private static final String AUTHORIZED = "AUTHORIZED"

    def setUserAuthorization(User user) {
        def authorization = [isLoggedIn: true, user: user]
        AppUtil.getAppSession()[AUTHORIZED] = authorization
    }

    def doLogin(String email, String password){
        password = password.encodeAsMD5()
        User user = User.findByEmailAndPassword(email, password)
        if (user){
            setUserAuthorization(user)
            return true
        }
        return false
    }

    boolean isAuthenticated(){
        def authorization = AppUtil.getAppSession()[AUTHORIZED]
        if (authorization && authorization.isLoggedIn){
            return true
        }
        return false
    }


    def getUser(){
        def authorization = AppUtil.getAppSession()[AUTHORIZED]
        return authorization?.user
    }


    def getUserName(){
        def user = getUser()
        return "${user.firstName} ${user.lastName}"
    }

    def isAdministratorUser(){
        def user = getUser()
        if (user && user.userType == GlobalConfig.USER_TYPE.ADMINISTRATOR){
            return true
        }
        return false
    }


    def save(GrailsParameterMap params) {
        User user = new User(params)
        user.birthDate = new SimpleDateFormat( 'yyyy-MM-dd' ).parse params.birthDate
        def response = AppUtil.saveResponse(false, user)
        if (user.validate()) {
            user.save(flush: true)
            if (!user.hasErrors()){
                response.isSuccess = true
            }
        }
        return response
    }


    def update(User user) {
        def response = AppUtil.saveResponse(false, user)
            user.save(flush: true)
            if (!user.hasErrors()){
                response.isSuccess = true
            }
        return response
    }

    def userList(){
        return User.list();
    }
}
