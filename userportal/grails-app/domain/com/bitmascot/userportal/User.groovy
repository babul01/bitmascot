package com.bitmascot.userportal

class User {

    Integer id
    String firstName
    String lastName
    String address
    String phone
    String email
    String birthDate
    String password

    String userType = GlobalConfig.USER_TYPE.REGULAR_MEMBER
//    Date dateCreated
//    Date lastUpdated

    static constraints = {
        firstName(nullable: false)
        lastName(nullable: true)
        address(nullable: true)
        phone(nullable: true)
        email(email: true, nullable: false, unique: true, blank: false)
        birthDate(nullable: false)
        password(blank: false)
    }

    def beforeInsert (){
        this.password = this.password.encodeAsMD5()
    }


    def beforeUpdate(){
        this.password = this.password.encodeAsMD5()
    }

    static mapping = {
        version(false)
    }
}
