package com.bitmascot.userportal

class UIHelperTagLib {
    static namespace = "UIHelper"

    UserService userService

    def userActionMenu = { attrs, body ->
        out << g.link(class:"nav-link dropdown-toggle text-white", id:"navbardrop", "data-toggle":"dropdown"){userService.getUserName()}
    }

    def leftNavigation = { attrs, body ->

        if(userService.isAdministratorUser()){
            out << g.link(controller:"dashboard", action:"userList", class:"sidenava border-bottom p-2 text-dark btn"){g.message(code:"user.list")}
        }else {
            out << g.link(controller:"dashboard", action:"index", class:"sidenava border-bottom p-2 text-dark btn"){g.message(code:"profile.page")}
            out << g.link(controller:"dashboard", action:"changePass", class:"sidenava border-bottom p-2 text-dark btn"){g.message(code:"change.pass")}
        }
    }
}
