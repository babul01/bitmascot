package com.bitmascot.userportal

class UserController {

    UserService userService

    def index(){}

    def doLogin() {
        if (userService.doLogin(params.email, params.password)) {
            if (userService.isAuthenticated() && userService.isAdministratorUser()) {
                redirect(controller: "dashboard", action: "userList")
            }else if (userService.isAuthenticated()){
                redirect(controller: "dashboard", action: "index")
            }else {
                redirect(controller: "user", action: "index")
            }
        } else {
            flash.message = AppUtil.infoMessage("Incorrect Email or Password. Try Again..", false)
            redirect(controller: "user", action: "index")
        }
    }

    def logout() {
        session.invalidate()
        redirect(controller: "user", action: "index")
    }

    def registration() {
        [user: flash.redirectParams]
    }

    def doRegistration() {
        def response = userService.save(params)
        if (response.isSuccess) {
            userService.setUserAuthorization(response.model as User)
            flash.message = AppUtil.infoMessage("Registration Successful", false)
            redirect(controller: "user", action: "index")
        } else {
            flash.message = AppUtil.infoMessage("Registration failed. Try Again..", false)
            redirect(controller: "user", action: "registration")
        }
    }

    def checkAvailable(){
        if(User.findByEmail(params.email)){
            render false;
        }else {
            render true;
        }
    }


}
