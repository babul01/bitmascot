package com.bitmascot.userportal

class DashboardController {
    UserService userService

    def index() {
        if (!userService.isAdministratorUser()) {
            def response = userService.getUser();
            if (!response) {
                redirect(controller: "user", action: "index")
            } else {
                [user: response]
            }
        } else {
            flash.message = AppUtil.infoMessage("You are not Authorized for this Action.", false)
            redirect(controller: "dashboard", action: "userList")
        }

    }

    def changePass() {}

    def doChangePass() {
        def user = userService.getUser()
        String newPassword = params.password.encodeAsMD5()
        String prePassword = params.prepassword.encodeAsMD5()

        if (prePassword.equals(user.password)) {
            int isChanged = User.executeUpdate("update User u set u.password=:newPassword " + "where u.email=:email",
                    [newPassword: newPassword, email: user.email])
            if (isChanged) {
                flash.message = AppUtil.infoMessage("Password changed successful", false)
                redirect(controller: "dashboard", action: "changePass")
            } else {
                flash.message = AppUtil.infoMessage("update failed", false)
                redirect(controller: "dashboard", action: "changePass")
            }
        } else {
            flash.message = AppUtil.infoMessage("Incorrect previous password", false)
            redirect(controller: "dashboard", action: "changePass")
        }
    }


    def userList() {
        if (userService.isAdministratorUser()) {
            def response = userService.userList()
            [users: response, total: response.count]
        } else {
            flash.message = AppUtil.infoMessage("You are not Authorized for this Action.", false)
            redirect(controller: "dashboard", action: "index")
        }
    }

}
