package com.bitmascot.userportal


class SecurityInterceptor {

    UserService userService

    SecurityInterceptor() {
        matchAll().excludes(controller: "user")
    }

    boolean before() {
        if (!userService.isAuthenticated()) {
            redirect(controller: "user", action: "index")
            return false
        }
        return true
    }


}
