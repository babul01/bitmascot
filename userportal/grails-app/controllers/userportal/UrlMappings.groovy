package userportal

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'user', action: 'index')
        "/registration"(controller: 'user', action: 'registration')
        "/profile"(controller: 'dashboard', action: 'index')
        "/change-password"(controller: 'dashboard', action: 'changePass')
        "/admin"(controller: 'dashboard', action: 'userList')

        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
