<%--
  Created by IntelliJ IDEA.
  User: babul
  Date: 9/7/19
  Time: 11:25 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>User Portal :: Profile</title>
    <meta name="layout" content="main">
</head>

<body>

<div class="main">
    <div class="row justify-content-center p-3 border-bottom">
        <h2>User Profile</h2>
    </div>

    <div class="row justify-content-center mt-5">
        <g:if test="${user}">
            <table class="table table-bordered w-50">
                <thead>

                </thead>
                <tbody>
                <tr>
                    <td>First Name</td>
                    <td>${user.firstName}</td>
                </tr>
                <tr>

                    <td>Last Name</td>
                    <td>${user.lastName}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>${user.address}</td>
                </tr>

                <tr>
                    <td>Phone</td>
                    <td>${user.phone}</td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td>${user.email}</td>
                </tr>

%{--                <tr>--}%
%{--                    <td>Birth Date</td>--}%
%{--                    <td>${user.birthDate}</td>--}%
%{--                </tr>--}%
                </tbody>
            </table>
        </g:if>
    </div>
</div>

</body>
</html>