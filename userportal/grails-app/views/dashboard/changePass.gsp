<%--
  Created by IntelliJ IDEA.
  User: babul
  Date: 9/8/19
  Time: 3:03 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>User Portal :: Change Password</title>
    <meta name="layout" content="main">
</head>

<body>

<div class="main">

<div class="row justify-content-center p-3 border-bottom">
    <h2>Change Password</h2>
</div>

<g:form controller="dashboard" action="doChangePass" method="post">
    <div class="border p-3 mx-auto w-50 mt-5">
        <div class="row justify-content-between mt-2">
            <div class="col-4">
                Previous Password
            </div>

            <div class="col-8">
                <input type="password" name="prepassword" value="" class="w-100">
            </div>
        </div>

        <div class="row justify-content-between mt-2">
            <div class="col-4">
                New Password
            </div>

            <div class="col-8">
                <input id="txtNewPassword" type="password" name="newpassword" value="" class="w-100">
            </div>
        </div>

        <div class="row justify-content-between mt-2">
            <div class="col-4">
                Confirm Password
            </div>

            <div class="col-8">
                <input id="txtConfirmPassword" type="password" name="password" value="" class="w-100" onkeyup="checkPasswordMatch();">
                <div id="divCheckPasswordMatch"></div>
            </div>
        </div>

        <div class="row justify-content-around mt-3">
            <g:submitButton name="change-pass" value="Change Password" class="btn btn-primary"/>
            <g:link controller="dashboard" action="changePass" class="btn btn-primary">Clear</g:link>
        </div>
    </div>
</g:form>
</div>

<script>
    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if (password != confirmPassword)
            $("#divCheckPasswordMatch").html("Passwords do not match!").css('color','red');
        else
            $("#divCheckPasswordMatch").html("Passwords match.").css('color','green');
    }

    $(document).ready(function () {
        $("#txtConfirmPassword").keyup(checkPasswordMatch);
    });

</script>
</body>
</html>