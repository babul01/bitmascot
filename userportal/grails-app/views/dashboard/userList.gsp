<%--
  Created by IntelliJ IDEA.
  User: babul
  Date: 9/8/19
  Time: 3:50 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>User Portal :: User List</title>
    <meta name="layout" content="main">
</head>

<body>

<div class="main">
    <div class="row justify-content-between p-3 border-bottom">
        <h2>User List</h2>

        <form class="form-inline mr-4" action="" method="post">
            <div class="form-group has-search">
                <g:img dir="images" file="ic_search.svg" class="form-control-feedback"/>
                <input id="search" type="text" class="form-control" placeholder="Search">
            </div>
        </form>

    </div>

    <div class="row justify-content-center">
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Age</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
            </tr>
            </thead>
            <tbody id="dataTable">
            <g:each in="${users}" var="info">
                <tr>
                    <th scope="row">${info?.firstName} ${info?.lastName}</th>
                    <td>${Calendar.getInstance().get(Calendar.YEAR)-(info?.birthDate.substring(info.birthDate.length()-4,info.birthDate.length()).toInteger())}</td>
                    <td>${info?.email}</td>
                    <td>${info?.phone}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#dataTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
</body>
</html>