<!doctype html>
<html>
<head>
    <title>User Portal :: Login</title>
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>

    <script type="text/javascript">
        <g:if test="${flash?.message && flash?.message?.info}">
        alert("${flash.message?.info}");
        </g:if>
    </script>

</head>

<body class="container-fluid">

<div class="row justify-content-center mt-5">
    <g:img dir="images" file="address-book.svg" width="40" height="40"/>
</div>

<div class="row justify-content-center">
    <h4>Login Panel</h4>
</div>

<g:form controller="user" action="doLogin" method="post">
    <div class="border p-3 mx-auto form-width">
        <div class="row justify-content-center">
            Email Address
        </div>

        <div class="row justify-content-center">
            <input type="email" name="email" class="w-75">
        </div>

        <div class="row justify-content-center mt-2">
            Password
        </div>

        <div class="row justify-content-center">
            <input type="password" name="password" class="w-75">
        </div>

        <div class="row justify-content-around mt-2">
            <g:submitButton name="login" value="Login" class="btn btn-primary"/>
            <g:link controller="user" action="index" class="btn btn-primary">Clear</g:link>
        </div>

        <div class="row justify-content-center mt-3">
            <p>Are you new here? <g:link controller="user" action="registration"><u>Registar Now</u></g:link></p>
        </div>

    </div>
</g:form>

</body>
</html>
