<!doctype html>
<html>
<head>
    <title>User Portal :: Registration</title>
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>

    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        <g:if test="${flash?.message && flash?.message?.info}">
        alert("${flash.message?.info}");
        </g:if>
    </script>

</head>

<body class="container-fluid">

<div class="row justify-content-center mt-5">
    <g:img dir="images" file="address-book.svg" width="40" height="40"/>
</div>

<div class="row justify-content-center">
    <h4>Registration Panel</h4>
</div>

<g:form controller="user" action="doRegistration" method="post">
    <div class="border p-3 mx-auto form-width">
        <div class="row justify-content-between">
            <div class="col-4">
                First Name
            </div>

            <div class="col-8">
                <input type="text" name="firstName" value="" class="w-100">
            </div>
        </div>

        <div class="row justify-content-between mt-2">
            <div class="col-4">
                Last Name
            </div>

            <div class="col-8">
                <input type="text" name="lastName" value="" class="w-100">
            </div>
        </div>

        <div class="row justify-content-between mt-2">
            <div class="col-4">
                Address
            </div>

            <div class="col-8">
                <input type="text" name="address" value="" class="w-100">
            </div>
        </div>

        <div class="row justify-content-between mt-2">
            <div class="col-4">
                Phone
            </div>

            <div class="col-8">
                <input type="text" name="phone" value="" class="w-100">
            </div>
        </div>

        <div class="row justify-content-between mt-2">
            <div class="col-4">
                Email
            </div>

            <div class="col-8">
                <input id="email" type="email" name="email" value="" class="w-100">
                <span id="msg"></span>
            </div>
        </div>

        <div class="row justify-content-between mt-2">
            <div class="col-4">
                Birth Date
            </div>

            <div class="col-8">
%{--                <input type="text" name="birthDate" value="" class="w-100">--}%
%{--                <g:datePicker name="birthDate" value="${new Date()}" noSelection="['':'-Choose-']"/>--}%
                <input id="datepicker" name="birthDate"/>
            </div>
        </div>

        <div class="row justify-content-between mt-2">
            <div class="col-4">
                Password
            </div>

            <div class="col-8">
                <input type="password" name="password" value="" class="w-100">
            </div>
        </div>

        <div class="row justify-content-around mt-2">
            <g:submitButton name="registar" value="Registar" class="btn btn-primary"/>
            <g:link controller="user" action="index" class="btn btn-primary">Cancel</g:link>
        </div>
    </div>
</g:form>

<script>
    $("#email").keyup(function () {
        var email = $("#email").val();
        if (!validateEmail(email)) {
            $("#msg").text("Invalid email address.").css("color", "red");;
        }
        else {
            isEmailAvailable(email);
        }
    });

    function validateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };

    function isEmailAvailable(email){
        $(document).ready(function(){
            var URL="${createLink(controller:'user',action:'checkAvailable')}";
            $.ajax({
                url:URL,
                method:'POST',
                data: {email:email},
                success: function(resp){
                    console.log(resp);
                    if(resp==='true'){
                        $("#msg").text("Your email is available").css("color", "green");
                    }else {
                        $("#msg").text("Your email is already taken").css("color", "red");
                    }
                }
            });
        });
    }
</script>
</body>
</html>
