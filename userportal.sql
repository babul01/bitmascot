-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 09, 2019 at 09:12 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `userportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `birth_date` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `phone`, `address`, `first_name`, `password`, `user_type`, `birth_date`, `last_name`, `email`) VALUES
(1, '01832825696', 'Panthapath, Dhaka', 'Abbas', 'e10adc3949ba59abbe56e057f20f883e', 'REGULAR_MEMBER', 'Thu Jan 01 00:00:00 BDT 1995', 'Uddin', 'abbas@gmail.com'),
(2, '01925101930', 'Dhanmondi, Dhaka', 'Siraj', 'e10adc3949ba59abbe56e057f20f883e', 'REGULAR_MEMBER', 'Fri Jan 01 00:00:00 BDT 1990', 'Khan', 'siraj@outlook.com'),
(3, '01925101033', 'Green Road, Dhaka', 'Rabbi', 'e10adc3949ba59abbe56e057f20f883e', 'REGULAR_MEMBER', 'Sat Jan 01 00:00:00 BDT 2000', 'Chowdhury', 'rabbi@gmail.com'),
(4, '02-8714743', 'H # 239, Rd # 17, Dhaka 1206', 'System', '21232f297a57a5a743894a0e4a801fc3', 'ADMINISTRATOR', 'Sun Jan 01 00:00:00 BDT 2006', 'Administrator', 'admin@localhost.local'),
(5, '01727351882', '55/3, Green Road, Dhaka', 'Babul', 'e10adc3949ba59abbe56e057f20f883e', 'REGULAR_MEMBER', 'Wed Aug 04 00:00:00 BDT 1993', 'Miah', 'babulcu35@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
